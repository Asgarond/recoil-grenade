# The Recoil Grenade

This project shall mimic the functions of the original grenade ( and perhaps add some more ... ).
The decision was taken as the grenades have become very expensive and rare. 
Perhaps because of that they have not found the way into every 3rd party app so far.

### The main functions are :

- immediate explosion ( #unassigned explosion ) that will generate no reward in the game for the owner
- assigned explosion where the grenade gets an ID (1-15) and the kill will be rewarded in the game score

Additionally I added a piezo buzzer that will give feedback on the status and of the countdown when being activated.

Base of the arrangement is a ESP8266 D1 Mini. I tried that first with a ATTiny85 Digispark but was getting mad with the 4KB flash restriction.
To get down from the 9V I use a adjustable (tiny) power control board (17x10mm).

All you will need for the sketch is the OneButton library which can be installed in IDE or downloaded here (https://github.com/mathertel/OneButton).

You might also watch the Youtube video that shows the feature of the program so far: https://youtu.be/2ox_1ypMxFo . 




2021, Andre West
