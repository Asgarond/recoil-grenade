/* This is the grenade code for the Recoil IR-protocol.
   Short press will detonate the grenade directly, long press will assignment it with a grenadeID to a registered Recoil gun in the game (kill reward to player).
   During explosion the process can be aborted by pressing the button (handled via interrupt).
   Base for this test was an ESP8266 WEMOS D1 Mini.
   You may use and develop this sketch at your wish.
   2021 - Andre West
*/

//----- included libraries -----
#include <OneButton.h>

//----- Timing Setup -----
unsigned long counterPreviousTime = 0;
unsigned long counterTime = 0;
unsigned long currentTime = millis();
const unsigned long countIntervall = 1000;

//----- OneButton Setup -----
const int PIN_BUTTON = D6;
OneButton button(PIN_BUTTON, true);
typedef enum { ACTION_NONE, ACTION_SINGLE, ACTION_DOUBLE, ACTION_LONGSTART, ACTION_PRESS, ACTION_LONGSTOP } MyActions;
MyActions nextAction = ACTION_NONE;

//----- Interrupt Setup -----
int grenExplode = 0;
ICACHE_RAM_ATTR void stopExplosion()
{
 if ( grenExplode == 1 ) allReset(); // interrupt and abort explosion
}

//----- IR Setup -----
const int IRLedPin = D5;

//---- Status LED ---
const int LedPin = D1;

String IR_Tag;
String IR_TagPart1;
int grenadeNo = 0;
int grenState = 5;                  // 0 = unassigned, 1 = begin assignment, 2 = end assigment, 3 = explosion, 4 = disarm, 5 = standby

//-----------------------------------------------------------------------------------------------------------
//----------------------------------------------void setup---------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void setup()
{
  pinMode( IRLedPin, OUTPUT);
  pinMode( LedPin, OUTPUT );

  Serial.begin( 115200 );           // Initialize Serial Console
  Serial.println();

  // ----- Initialize Interrupt -----
  attachInterrupt(digitalPinToInterrupt(PIN_BUTTON), stopExplosion, CHANGE);

  //----- Initialize OneButton -----
  button.setPressTicks( 1000 );
  button.setClickTicks( 600 );
  button.setDebounceTicks( 50 );
  button.attachClick( singleclick );
  button.attachDoubleClick( doubleclick );
  button.attachLongPressStart( longPressStart );

  grenadeStartSound();
  grenadeReady();

}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- void loop ---------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void loop()
{
  button.tick();                                // watching the button 1
  currentTime = millis();

  if ( grenState == 5 )                         // if grenade is in standby flash LED every 3 seconds
  {
    if ( currentTime - counterPreviousTime > countIntervall * 3)
    {
      digitalWrite( LedPin, HIGH );
      delay(50);
      digitalWrite( LedPin, LOW );
      Serial.println( "grenade on stand-by" );
      counterPreviousTime = currentTime;
    }
  }
  else if ( grenState == 3 )                    // if grenade is assigned keep LED on steadily
  {
    digitalWrite ( LedPin, HIGH );
  }

  if ( nextAction == ACTION_SINGLE )            // click button
  {
    if ( grenState == 5 ) grenState = 0;        // switch to unassigned explosion
    nextAction = ACTION_NONE;
    countdown();
    grenadeExplode();
  }

  if ( nextAction == ACTION_LONGSTART )         // hold button
  {
    if ( grenState == 5 ) grenadeAssign();      // if in standby goto assign grenade

    else if ( grenState == 3 ) allReset();      // if already assigned, reset grenade to standby

    nextAction = ACTION_NONE;
  }
}

//-----------------------------------------------------------------------------------------------------------
//-------------------------------------------- Menu subroutines ---------------------------------------------
void grenadeExplode()
{
  grenExplode = 1;
  for ( int i = 0; i < 55; i++ )
  {
    if ( grenState == 0 )                       // unassigned grenade explode
    {
      digitalWrite( LedPin, HIGH );
      IR_Tag = "110000001001011110111";
      SendLaserCode( IR_Tag );                  // 110000001001011110111
      digitalWrite( LedPin, LOW );
      Serial.println( "unassigned grenade explosion " );
    }
    else if ( grenState == 3 )                 // assigned grenade explode
    {
      digitalWrite( LedPin, HIGH );
      calcIR_Tag();
      SendLaserCode( IR_Tag );
      digitalWrite( LedPin, LOW );
      Serial.print( "explosion with grenade no. " );
      Serial.println( grenadeNo );
    }
    delay(500);                               // delay between grenade shots
  }
  delay(200);
  grenState = 4;                              // send disarm-code
  calcIR_Tag();
  SendLaserCode( IR_Tag );
  Serial.println( "grenade disarmed " );

  allReset();                                 // resetting grenade number and setting grenade state to standby
}

//-----------------------------------------------------------------------------------------------------------
void grenadeAssign()
{
  grenState = 1;
  digitalWrite( LedPin, HIGH );
  SendLaserCode( "010010001001011110010" );   // 010010001001011110010 - begin assignment
  delay(500);
  Serial.println( "assigning grenade " );
  grenState = 2;

  grenadeNo = random(1, 16);                  // assing a random number between 1 and 15
  calcIR_Tag();
  SendLaserCode( IR_Tag );                    // send assign code to Recoil gun
  Serial.print( "grenade assigned with " );
  Serial.print( IR_Tag );
  Serial.print( " - grenade no.  " );
  Serial.println( grenadeNo );
  grenState = 3;

  for ( int i = 0; i < 3; i++ )               // audible confirmation of assignment - 3 beeps (di-di-dit)
  {
    tone(D7, 4000 );
    delay(50);
    noTone( D7 );
    delay(100);
  }
  digitalWrite( LedPin, LOW );
}

//-----------------------------------------------------------------------------------------------------------
void countdown()                              // 10 seconds
{
  digitalWrite( LedPin, LOW );                
  delay(100);
  Serial.println( "starting countdown" );
  
  for ( int i = 0; i < 540; i+=20 )          // accelerating countdown
  {
    digitalWrite( LedPin, HIGH );
    tone(D7, 4000);
    delay(50-(i/30));
    digitalWrite( LedPin, LOW );
    noTone(D7);
    delay(560-i);
  }
}

//-----------------------------------------------------------------------------------------------------------
//----------------------------------------- OneButton subroutines -------------------------------------------
void singleclick()
{
  nextAction = ACTION_SINGLE;
}

//-----------------------------------------------------------------------------------------------------------
void doubleclick()
{
  nextAction = ACTION_DOUBLE;
}

//-----------------------------------------------------------------------------------------------------------
void longPressStart()
{
  nextAction = ACTION_LONGSTART;
}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- Tag Generator -----------------------------------------------
void calcIR_Tag()
{
  String actionCode;
  if ( grenState == 1 ) actionCode = "0100";      // begin assignment
  else if ( grenState == 2 ) actionCode = "1000"; // end assignment
  else if ( grenState == 3 ) actionCode = "1100"; // explosion
  else if ( grenState == 4 ) actionCode = "1110"; // disarm

  String grenadeID;
  if ( grenadeNo == 0 ) grenadeID = "10001";
  else if ( grenadeNo == 1 ) grenadeID = "00000";
  else if ( grenadeNo == 2 ) grenadeID = "00011";
  else if ( grenadeNo == 3 ) grenadeID = "00101";
  else if ( grenadeNo == 4 ) grenadeID = "00110";
  else if ( grenadeNo == 5 ) grenadeID = "01001";
  else if ( grenadeNo == 6 ) grenadeID = "01010";
  else if ( grenadeNo == 7 ) grenadeID = "01100";
  else if ( grenadeNo == 8 ) grenadeID = "01111";
  else if ( grenadeNo == 9 ) grenadeID = "10010";
  else if ( grenadeNo == 10 ) grenadeID = "10100";
  else if ( grenadeNo == 11 ) grenadeID = "10111";
  else if ( grenadeNo == 12 ) grenadeID = "11000";
  else if ( grenadeNo == 13 ) grenadeID = "11011";
  else if ( grenadeNo == 14 ) grenadeID = "11101";
  else if ( grenadeNo == 15 ) grenadeID = "11110";

  String constantGrenade = "00101111";

  String redunCheck_r = "10111110000001010";
  String redunCheck_s = "11100000001000011";
  String redunCheck_t = "10001110100101101";
  String redunCheck_u = "00101111001100100";

  String r;
  String s;
  String t;
  String u;
  int c = 0;

  if ( grenState == 3)                                        // invert bit "E" for expplosion
  {
    if ( grenadeID[0] == '0' ) grenadeID[0] = '1';
    else if ( grenadeID[0] == '1' ) grenadeID[0] = '0';
  }
  
  IR_TagPart1 = actionCode + grenadeID + constantGrenade;     // partial tag for grenade

  for ( int i = 0; i < IR_TagPart1.length(); i++ )            // reduncancy check for last 4 bits RSTU
  {
    if ( IR_TagPart1[i] == redunCheck_r[i] && IR_TagPart1[i] == '1' ) c++;
    if ( i == 16  && c % 2 == 0) r = "1";
    else if ( i == 16  && c % 2 > 0) r = "0";
  }
  c = 0;

  for ( int i = 0; i < IR_TagPart1.length(); i++ )
  {
    if ( IR_TagPart1[i] == redunCheck_s[i] && IR_TagPart1[i] == '1' ) c++;
    if ( i == 16  && c % 2 == 0) s = "1";
    else if ( i == 16  && c % 2 > 0) s = "0";
  }
  c = 0;

  for ( int i = 0; i < IR_TagPart1.length(); i++ )
  {
    if ( IR_TagPart1[i] == redunCheck_t[i] && IR_TagPart1[i] == '1' ) c++;
    if ( i == 16  && c % 2 == 0) t = "1";
    else if ( i == 16  && c % 2 > 0) t = "0";
  }
  c = 0;

  for ( int i = 0; i < IR_TagPart1.length(); i++ )
  {
    if ( IR_TagPart1[i] == redunCheck_u[i] && IR_TagPart1[i] == '1' ) c++;
    if ( i == 16  && c % 2 == 0) u = "1";
    else if ( i == 16  && c % 2 > 0) u = "0";
  }

  IR_Tag = IR_TagPart1 + r + s + t + u;                       // final tag for grenade
}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- IR subroutines ----------------------------------------------
void SendLaserCode( String IR_Tag )                          // sending code from a string
{
  for ( int c = 0; c < 2; c++)                               // repetition as reduntant shot
  {
    pulseIR(3200);                                           // header and
    delayMicroseconds(1600);                                 // gap after header to initialize shot

    int revertCode = 0;
    for ( int i = 0; i < IR_Tag.length(); i++ )
    {
      if ( IR_Tag[i] == '0' && revertCode == 0 )
      {
        pulseIR(400);
        delayMicroseconds(400);
      }
      else if ( IR_Tag[i] == '1' && revertCode == 0 )
      {
        pulseIR(800);
        revertCode = 1;
      }
      else if ( IR_Tag[i] == '0' && revertCode == 1 )
      {
        delayMicroseconds(400);
        pulseIR(400);
      }
      else if ( IR_Tag[i] == '1' && revertCode == 1 )
      {
        delayMicroseconds(800);
        revertCode = 0;
      }
    }
    delayMicroseconds(5000);
  }
}

//-----------------------------------------------------------------------------------------------------------
void pulseIR(int microsecs)         // sending the pulse for the defined number of microseconds
{
  tone( IRLedPin, 38000);
  delayMicroseconds( microsecs );
  noTone( IRLedPin );
}

//-----------------------------------------------------------------------------------------------------------
void grenadeStartSound()
{
 for ( int i = 1000; i<4000; i+=2 ) 
 {
  tone(D7, i);
  delay(1);
 }
 noTone(D7);
 delay(100);
}

//-----------------------------------------------------------------------------------------------------------
void grenadeReady()
{
  digitalWrite( LedPin, HIGH );
  tone(D7, 4000 );
  delay(100);
  noTone( D7 );
  digitalWrite( LedPin, LOW );
  delay(100);
  digitalWrite( LedPin, HIGH );
  tone(D7, 4000 );
  delay(50);
  noTone( D7 );
  digitalWrite( LedPin, LOW );
  Serial.println( "grenade ready" );
}

//-----------------------------------------------------------------------------------------------------------
void allReset()
{
  grenadeReady();
  grenadeNo = 0;
  grenState = 5;
  grenExplode = 0;
}
